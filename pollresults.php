<?php
/**
 * Plugin Name:     Pollresults
 * Plugin URI:      https://zoo.nl
 * Description:     Show poll results from Poll Maker plugin
 * Author:          Ronnie Stevens
 * Author URI:      https://ronniestevens.co
 * Text Domain:     pollresults
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Pollresults
 */

// Your code starts here.

function poll_results_admin_menu() {
    add_menu_page(
        __( 'Poll Results', 'pollresults' ),
        __( 'Poll Results', 'pollresults' ),
        'manage_options',
        'poll-results',
        'poll_results_admin_page_contents',
        'dashicons-schedule',
        3
    );
}

add_action( 'admin_menu', 'poll_results_admin_menu' );


function poll_results_admin_page_contents() {
    ?>
        <h1>
            <?php esc_html_e( 'Poll Results.', 'my-plugin-textdomain' ); ?>
        </h1>
    <?php
    show_poll_results();
}

function show_poll_results() {
    global $wpdb;
    $result = $wpdb->get_results('SELECT answer, votes FROM wp_ayspoll_answers ORDER BY votes');
    $total = $wpdb->get_var('SELECT SUM(votes) as value FROM wp_ayspoll_answers');
    echo '<style type="text/css">';
    echo 'td{border:solid 1px #cccccc;padding:0.5rem 1rem;}';
    echo '.right{text-align:right;}';
    echo '</style>';
    echo '<table>';
    foreach($result as $row) {
        echo '<tr>';
        echo '<td>' . $row->answer . ':</td>'; 
        echo '<td class="right">' . $row->votes . '</td>';
        echo '</tr>';
    }
    echo '<tr><td><b>Total:</b></td><td><b>'. $total . '</b></td></tr>';
    echo '</table>';

}
